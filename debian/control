Source: plainbox-provider-piglit
Section: utils
Priority: optional
Maintainer: Checkbox Developers <checkbox-devel@lists.ubuntu.com>
Uploaders: Zygmunt Krynicki <zygmunt.krynicki@canonical.com>,
           Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 9),
               intltool,
               plainbox-provider-resource-generic (>= 0.17),
               python3,
               python3-plainbox (>= 0.20)
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/python-team/packages/plainbox-provider-piglit.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/plainbox-provider-piglit
Homepage: http://launchpad.net/plainbox-provider-piglit

Package: plainbox-provider-piglit
Architecture: all
Depends: piglit (>= 0~git201503121),
         plainbox-provider-resource-generic (>= 0.17),
         python3 (>= 3.2),
         python3-plainbox (>= 0.20),
         ${misc:Depends}
Description: Piglit (OpenGL/OpenCL) Test Provider for Plainbox
 This package contains a plainbox provider, a set of tests (called jobs)
 that can be executed with the plainbox toolkit or applications based
 on plainbox.
 .
 This providers wraps several of piglit tests and exposes them as plainbox
 jobs. There are also jobs for creating a piglit-made HTML report and packaging
 it as a tarball.
 .
 The full list of units contained in this provider is:
 .
 2013.com.canonical.certification::piglit/test/fbo
 2013.com.canonical.certification::piglit/test/gl-2.1
 2013.com.canonical.certification::piglit/test/vbo
 2013.com.canonical.certification::piglit/test/glsl-fragment-shader
 2013.com.canonical.certification::piglit/test/glsl-vertex-shader
 2013.com.canonical.certification::piglit/test/glx-tfp
 2013.com.canonical.certification::piglit/test/stencil_buffer
 2013.com.canonical.certification::piglit/support/combine_results
 2013.com.canonical.certification::piglit/support/summarize_results
 2013.com.canonical.certification::piglit/support/tarball
 2013.com.canonical.certification::__piglit__
 .
 They can be executed together using this test plan:
 .
 2013.com.canonical.certification::piglit
